# Codesys_JSON_hvakosterstrommen

Use Wago e!Cockpit.

Use WagoAppTime, WagoAppJSON and WagoAppHTTP.


The goal of the program is to obtain electricity prices from 
'www.hvakosterstrommen.no/api' 
And put them in variables that can be used further in codesys.

Info about api: https://www.hvakosterstrommen.no/strompris-api
Exampel Json: https://www.hvakosterstrommen.no/api/v1/prices/2022/10-28_NO1.json

